﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SecretSantaGenerator
{
    public class SecretSantaGenerator
    {
        public List<SecretSanta> MasterList { get; set; }
        public List<SecretSanta> Assignments { get; set; }
        public List<int> AvailableMatches { get; set; }

        private string _sendEmail = "osodigitalinc@gmail.com";
        private string _sendEmailPassword = "Realf8th";

        private Random rnd;

        public SecretSantaGenerator()
        {
            initLists();
            rnd = new Random();
        }

        public void RunSecretSanta()
        {
            RandomizeAssignments();
            SendAssignmentEmails();
            System.IO.File.WriteAllLines("SantaAssignments - Ran on " + DateTime.Now.ToString("MM-dd-yyyy HH.mm.ss") + ".txt", PrintAssignments());
        }

        private void RandomizeAssignments()
        {
            foreach (SecretSanta santa in Assignments)
            {
                int assignment = GetAssignment(santa.Id);
                santa.DrawnSanta = GetSanta(assignment);
            }

            GetSanta(2).DrawnSanta = GetSanta(3);
            Assignments.Add(GetSanta(2));

            if (!AssignmentsValid())
            {
                Refresh();
                RandomizeAssignments();
            }
        }

        private void SendAssignmentEmails()
        {
            if (Assignments != null)
            {
                string emailBody;
                foreach (SecretSanta santa in Assignments)
                {
                    emailBody =
                        _emailBody
                        .Replace(EmailMergeFields.DrawnName.ToString(), santa.DrawnSanta.Name)
                        .Replace(EmailMergeFields.FirstName.ToString(), santa.Name)
                        .Replace(EmailMergeFields.ImageUrl.ToString(), _imageHost + santa.DrawnSanta.ProfilePicUrl);

                    SendEmail(santa.Email, santa.Name, "Your Oso Secret Santa Assignment!!", emailBody);
                }
            }
        }

        private bool SendEmail(string toAddress, string toName, string subject, string messageBody)
        {
            try
            {
                MailAddress mailFrom = new MailAddress(_sendEmail, "Oso Secret Santa");

                MailMessage newmsg = new MailMessage();
                newmsg.From = mailFrom;

                newmsg.Subject = subject;
                newmsg.Body = messageBody;
                newmsg.IsBodyHtml = true;
                newmsg.To.Add(new MailAddress(toAddress, toName));

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_sendEmail, _sendEmailPassword);
                smtp.EnableSsl = true;
                smtp.Send(newmsg);
                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                return false;

            }
        }

        private bool AssignmentsValid()
        {
            foreach (SecretSanta santa in Assignments)
            {
                if (santa.Id == santa.DrawnSanta.Id)
                {
                    //Console.WriteLine("Randomization Failed");
                    return false;
                }
            }
            //Console.WriteLine("Randomization Succeeded");
            return true;
        }

        public List<string> PrintAssignments()
        {
            List<string> output = new List<string>();
            output.Add("Matching are as follows:");

            foreach (SecretSanta santa in Assignments)
            {
                output.Add(santa.ToDrawnString());
            }

            output.Add("Merry Christmas!!!");

            return output;
        }

        private SecretSanta GetSanta(int assignment)
        {
            //return the a SecretSanta matching the SecretSanta with Id == assignment
            SecretSanta returns = MasterList.Where(santa => santa.Id == assignment).First();
            return returns;
        }

        private int GetAssignment(int santaId)
        {
            //Prevent Deadlock situation if there is only one available by intentionally creating improper assignent.
            if (AvailableMatches.Count == 1 && AvailableMatches.Contains(santaId))
            {
                //Console.WriteLine("Deadlock being avoided.Deadlock being avoided.Deadlock being avoided.Deadlock being avoided.Deadlock being avoided.Deadlock being avoided.Deadlock being avoided.Deadlock being avoided.");
                return santaId;
            }

            //Get a random assignment 1-5
            int assignment = rnd.Next(5) + 1;

            //Continue to regenerate a random assingment until it's availabe
            while (!(AvailableMatches.Contains(assignment)) || (santaId == assignment))
            {
                assignment = rnd.Next(5) + 1;
            }

            //remove the assignment from the available matches
            AvailableMatches.Remove(assignment);

            return assignment;
        }

        private void initLists()
        {
            //Initialize list objects
            MasterList = new List<SecretSanta>();
            Assignments = new List<SecretSanta>();

            //Initilize the SecretSantas
            SecretSanta Obafemi = new SecretSanta(1, "Obafemi", "obafemioso@gmail.com", "Obafemi.jpg");
            SecretSanta Kemi = new SecretSanta(2, "Kemi", "absurd.kemi@gmail.com", "Kemi.jpg");
            SecretSanta Jemima = new SecretSanta(3, "Jemima", "jemimao@stanford.edu", "Jemima.jpg");
            SecretSanta Manasseh = new SecretSanta(4, "Manasseh", "manassehoso@gmail.com", "Manasseh.jpg");
            SecretSanta Remi = new SecretSanta(5, "Remi", "remioso@stanford.edu", "Remi.jpg");

            //Add Santas to the master list
            MasterList.Add(Obafemi);
            MasterList.Add(Kemi);
            MasterList.Add(Jemima);
            MasterList.Add(Manasseh);
            MasterList.Add(Remi);

            //Add Santas to Assignments list minus customization
            Assignments.Add(Obafemi);
            Assignments.Add(Jemima);
            Assignments.Add(Manasseh);
            Assignments.Add(Remi);

            AvailableMatches = new List<int>(new int[] { 1, 2, 4, 5 });
        }

        public void Refresh()
        {
            initLists();
        }

        private string _imageHost = "http://obafemioso.com/SecretSanta/";
        private string _emailBody = "<html lang=\"en\"><body> <div id=\":l0\" class=\"ii gt m14a40b2df968fadc adP adO\"> <div id=\":kz\" class=\"a3s\" style=\"overflow: hidden;\"> <u></u> <div marginwidth=\"0\" marginheight=\"0\" style=\"margin:0;padding:0;background-color:#FFFFFF;min-height:100%!important;width:100%!important\"> <center> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"border-collapse:collapse;margin:0;padding:0;background-color:#FFFFFF;height:100%!important;width:100%!important\"> <tbody> <tr> <td align=\"center\" valign=\"top\" style=\"margin:0;padding:20px;border-top:0;height:100%!important;width:100%!important\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;border:0\"> <tbody> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;background-color:#f8f8f8;border-top:0;border-bottom:0\"> <tbody> <tr> <td valign=\"top\" style=\"padding-top:9px\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse\"> <tbody> <tr> <td valign=\"top\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"366\" style=\"border-collapse:collapse\"> <tbody> <tr> <td valign=\"top\" style=\"padding:9px 0px 9px 18px;color:#474747;font-family:Helvetica;font-size:11px;line-height:125%;text-align:left\"> And your Oso Secret Santa Assignment is... </td> </tr> </tbody> </table> <table align=\"right\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"197\" style=\"border-collapse:collapse\"> <tbody> <tr> <td valign=\"top\" style=\"padding:9px 18px 9px 0px;color:#474747;font-family:Helvetica;font-size:11px;line-height:125%;text-align:left\"> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0\"> <tbody> <tr> <td valign=\"top\"></td> </tr> </tbody> </table> </td> </tr> <tr> <td align=\"center\" valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border-collapse:collapse;background-color:#f8f8f8;border-top:0;border-bottom:0\"> <tbody> <tr> <td valign=\"top\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse\"> <tbody> <tr> <td valign=\"top\" style=\"padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px\"> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse\"> <tbody> <tr> <td valign=\"top\" style=\"padding:0\"> <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse\"> <tbody> <tr> <td valign=\"top\" style=\"padding-top:0px;padding-right:0;padding-bottom:0px;padding-left:0px\"> <img alt=\"\" src=\"%%IMAGEURL%%\" width=\"282\" style=\"max-width:683px;border:0;outline:none;text-decoration:none;vertical-align:bottom\" class=\"CToWUd a6T\" tabindex=\"0\"> <div class=\"a6S\" dir=\"ltr\" style=\"opacity: 1; left: 272px; top: 424px;\"> <div id=\":m7\" class=\"T-I J-J5-Ji aQv T-I-ax7 L3 a5q\" role=\"button\" tabindex=\"0\" aria-label=\"Download attachment \" data-tooltip-class=\"a1V\" data-tooltip=\"Download\"> <div class=\"aSK J-J5-Ji aYr\"></div> </div> </div> </td> </tr> </tbody> </table> <table align=\"right\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"264\" style=\"border-collapse:collapse\"> <tbody> <tr> <td valign=\"top\" style=\"padding-right:18px;padding-top:18px;padding-bottom:18px;color:#3f3f3f;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left\"> <h1 style=\"line-height:20.7999992370605px;margin:0;padding:0;display:block;font-family:Helvetica;font-size:40px;font-style:normal;font-weight:bold;letter-spacing:-1px;text-align:left;color:#e6e6e6!important\"><span style=\"color:#2f4f4f\"><em>Your Secret<br> <br> Santa is:</em></span> </h1> <h3 style=\"line-height:20.7999992370605px;margin:0;padding:0;display:block;font-family:Helvetica;font-size:18px;font-style:normal;font-weight:bold;letter-spacing:-.5px;text-align:left;color:#e6e6e6!important\"><br> <span style=\"color:#2f4f4f\">%%DRAWNNAME%%</span> </h3> <p style=\"line-height:20.7999992370605px;margin:1em 0;padding:0;color:#e6e6e6;font-family:Helvetica;font-size:15px;text-align:left\"><span style=\"color:#2f4f4f\">Hey %%FIRSTNAME%%<br> <br> <span style=\"font-family:helvetica,arial,lucida grande,tahoma,verdana,arial,sans-serif;font-size:14px;line-height:19.3199996948242px\"> Now that you know who you have, visit our trello board for eveyone's wishlist to get some ideas and happy gifting!!!<br> <br> Remember the budget is $25, so don't feel the need to go buying a PS4 for Remi or anything. ;p<br> <br> More important though, it just the thought of giving a gift that matters and we'll all be appreciative no matter what!<br> <br> I love you and Merry Christmas!!! <3 <3 <3 </span></span> </p> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse\"> <tbody> <tr> <td valign=\"top\"> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td align=\"center\" valign=\"top\"> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </center> <img src=\"https://ci6.googleusercontent.com/proxy/IkFD9t9VrJDWuCnZK_haMQhknCOo4iBLbAo23sEzUGwcL8m8_n9pb8zbBv7bUPw2KckjSj6eWR8K2HQ1vxx38WHwlFjCecFRUdK7pd8wAVDhMscxRYUx3C_pDOUdSpXwNSihct98mTsSqRChIYXqXV9UxGq8hzjiXz6Iwa5xig=s0-d-e1-ft#http://facebook.us7.list-manage.com/track/open.php?u=43725bbca0746d1fc7350a5bb&amp;id=989a59c88b&amp;e=cdeab3b17e\" height=\"1\" width=\"1\" class=\"CToWUd\"> </div> <div class=\"yj6qo\"></div> <div class=\"adL\"></div> </div> </div> </body></html>";
    }

    public static class EmailMergeFields
    {
        public static string DrawnName = "%%DRAWNNAME%%";
        public static string FirstName = "%%FIRSTNAME%%";
        public static string ImageUrl = "%%IMAGEURL%%";
    }
}
