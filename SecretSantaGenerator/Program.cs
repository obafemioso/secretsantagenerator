﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretSantaGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            SecretSantaGenerator generator = new SecretSantaGenerator();

            generator.RunSecretSanta();
            Console.WriteLine("Secret Santa Deployed!");
            Console.ReadLine();
        }
    }
}
