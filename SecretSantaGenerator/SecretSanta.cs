﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecretSantaGenerator
{
    public class SecretSanta
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public SecretSanta DrawnSanta { get; set; }
        public string Email { get; set; }
        public List<string> WishList { get; set; }

        public string ProfilePicUrl { get; set; }

        public SecretSanta(int id, string name, string email, string profileUrl)
        {
            Id = id;
            Name = name;
            Email = email;
            ProfilePicUrl = profileUrl;
            DrawnSanta = null;
        }

        public string ToDrawnString()
        {
            return Name + " has drawn " + DrawnSanta.Name + "!";
        }

        public override string ToString()
        {
            return "Id: " + Id + ", Name: " + Name + ", DrawnSanta: " + DrawnSanta.Name + ", Email: " + Email;
        }
    }
}
